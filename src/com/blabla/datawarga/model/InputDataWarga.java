/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blabla.datawarga.model;

/**
 *
 * @author HP
 */
public class InputDataWarga {
    private String nama;
    private String ttl;
    private String jeniskl;
    private String pekerjaan;
    private String agama;
    private String statusprk;
    private String kerwarganegaraan;
    private String alamat;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getJeniskl() {
        return jeniskl;
    }

    public void setJeniskl(String jeniskl) {
        this.jeniskl = jeniskl;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getStatusprk() {
        return statusprk;
    }

    public void setStatusprk(String statusprk) {
        this.statusprk = statusprk;
    }

    public String getKerwarganegaraan() {
        return kerwarganegaraan;
    }

    public void setKerwarganegaraan(String kerwarganegaraan) {
        this.kerwarganegaraan = kerwarganegaraan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    
    
}
