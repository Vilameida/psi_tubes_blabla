/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blabla.datawarga.ui;

import java.sql.ResultSet;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author HP
 */
public class TabelDataWargaFrame extends javax.swing.JFrame {

    /**
     * Creates new form TabelDataWarga
     */
     
    public static int statusSearching = 0;
    public TabelDataWargaFrame() {
        initComponents();
        DefaultTableModel model = (DefaultTableModel) tblData.getModel();
    }
    FormDataWargaFrame fp = new FormDataWargaFrame();
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblData = new javax.swing.JTable();
        txtCari = new javax.swing.JTextField();
        btnCari = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Tabel Data Warga");

        tblData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Aisyah", "232454789", "Bandung/22 Maret 1974", "Perempuan", "Islam", "PNS", "Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"},
                {"Siti", "232454688", "Bandung/12 April 1977", "Perempuan", "Islam", "Ibu Rumah Tangga", "Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"},
                {"Jonatan", "232454587", "Bandung/2 Mei 1966", "Laki-laki", "Kristen", "Karyawan Swasta", "Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"},
                {"Iis", "232454486", "Subang/20 Juni 1970", "Perempuan", "Islam", "Karyawan Pabrik", "Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"},
                {"Glen", "232454385", "Cirebon/11 Januari 1983", "Laki-laki", "Kristen", "Karyawan Swasta", "Belum Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"},
                {"Erika", "232454284", "Sumedang/10 Desember 1968", "Perempuan", "Kristen", "PNS", "Belum Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"},
                {"Dian", "232454183", "Semarang/28 Februari 1976", "Perempuan", "Islam", "PNS", "Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"},
                {"Wawani", "232454082", "Subang/19 November 1969", "Laki-laki", "Islam", "Wiraswasta", "Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"},
                {"Kusnandar", "232453981", "Bandung/30 Oktober 1982", "Laki-laki", "Islam", "Honorer", "Belum Menikah", "Indonesia", "Kp.Cirateun RT.RW/01.03 Desa.Cirateun Kecamatan Cirateun Kabupaten Bandung Kota Bandung"}
            },
            new String [] {
                "Nama", "NIK", "Tempat / Tanggal Lahir", "Jenis Kelamin", "Agama", "Pekerjaan", "Status Perkawinan", "Kewarganegaraan", "Alamat"
            }
        ));
        tblData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDataMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblData);

        txtCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCariKeyReleased(evt);
            }
        });

        btnCari.setText("Cari");
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCari)
                .addGap(164, 164, 164)
                .addComponent(jLabel1)
                .addContainerGap(508, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCari))))
                .addContainerGap(248, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(59, 59, 59)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(60, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDataMouseClicked
        // TODO add your handling code here:
        int index = tblData.getSelectedRow();
        TableModel model = tblData.getModel();

        String nama = model.getValueAt(index, 0).toString();
        String nik = model.getValueAt(index, 1).toString();
        String ttl = model.getValueAt(index, 2).toString();
        String jeniskl = model.getValueAt(index, 3).toString();
        String agama = model.getValueAt(index, 4).toString();
        String pekerjaan = model.getValueAt(index, 5).toString();
        String statusprk = model.getValueAt(index, 6).toString();
        String kewarganegaraan = model.getValueAt(index, 7).toString();
        String alamat = model.getValueAt(index, 8).toString();

        fp.setVisible(true);
        fp.pack();
        fp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        fp.txtNama.setText(nama);
        fp.txtNik.setText(nik);
        fp.txtTtl.setText(ttl);
        fp.txtJeniskl.setText(jeniskl);
        fp.txtAgama.setText(agama);
        fp.txtPekerjaan.setText(pekerjaan);
        fp.txtStatusprk.setText(statusprk);
        fp.txtKewarganegaraan.setText(kewarganegaraan);
        fp.txaAlamat.setText(alamat);
    }//GEN-LAST:event_tblDataMouseClicked

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCariActionPerformed

    private void txtCariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCariKeyReleased
        // TODO add your handling code here:
        String st = txtCari.getText().trim();
    }//GEN-LAST:event_txtCariKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TabelDataWargaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TabelDataWargaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TabelDataWargaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TabelDataWargaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TabelDataWargaFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCari;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblData;
    private javax.swing.JTextField txtCari;
    // End of variables declaration//GEN-END:variables
   
}
