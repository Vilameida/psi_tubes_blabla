/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blabla.datawarga.controller;

import com.blabla.datawarga.model.InputDataWarga;
import com.blabla.datawarga.ui.FormDataWargaFrame;

/**
 *
 * @author HP
 */
public class InputDataWargaBean {
    private FormDataWargaFrame form;
    private InputDataWarga input;
    private InputAccessDatabase dao;
    
    public InputDataWargaBean(FormDataWargaFrame form, InputDataWarga input){
        this.form = form;
        this.input = input;
        
        dao = new InputAccessDatabase();
    }
    public void inputData(){
        dao.save(input);
    }
}
